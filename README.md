# AES test for A-Core

You can import this test to `ACoreTests` by adding the following option when running `configure`:
```
cd a-core_thesydekick/Entities/ACoreTests
./configure -t path/to/aes-test.yml
```