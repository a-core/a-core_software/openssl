#!/usr/bin/env bash
#Selective initialization of submodules
#Written by by Marko Kosunen, marko.kosunen@aalto.fi, 2017
DIR=$( cd `dirname $0` && pwd )
SUBMODULES="\
        ./openssl \
        "

echo ""
echo "Clones only the top level of openssl repo, not"
echo "the submodules, they are huge and not needed!"
echo ""
for mod in $SUBMODULES; do
        git submodule update --init $mod
            cd ${mod}
            if [ -f ./init_submodules.sh ]; then
                ./init_submodules.sh
            fi
            cd ${DIR}
done
exit 0

