#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "a-core-utils.h"
#include "a-core.h"
#include "acore-gpio.h"

// statically initialize some data in .data section
int result = 3;

// compute factorial of an integer recursively
int factorial(int n) {
    if (n == 0)
        return 1;
    else
        return n*factorial(n-1);
}

void main() {

    // Count program time
    clock_t start_t, end_t;
    start_t = clock();
    uint64_t start_instret, end_instret;
    start_instret = get_instret();

    // Init UART
    volatile uint32_t* uart_base_addr = (volatile uint32_t*) A_CORE_AXI4LUART;
    init_uart(uart_base_addr, BAUDRATE);
    
    test_pass();
}
