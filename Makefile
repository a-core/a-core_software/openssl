XLEN ?= 32

src_dir := openssl
aes_dir := $(src_dir)/crypto/aes
modes_dir := $(src_dir)/crypto/modes

default: all

aes_src_files = \
	$(aes_dir)/aes_ecb.c \
	$(aes_dir)/aes_core.c \
	# $(aes_dir)/aes_cbc.c \
	# $(aes_dir)/aes_ige.c \
	# $(aes_dir)/aes_misc.c \
	# $(aes_dir)/aes_ofb.c \
	# $(aes_dir)/aes_wrap.c \
	# $(wildcard $(modes_dir)/*.c)

common_src_files = $(wildcard ./env_acore/*.s) $(wildcard ./*.c)

#--------------------------------------------------------------------
# Build rules
#--------------------------------------------------------------------

MARCH ?= rv32im
MABI ?= ilp32

RISCV_PREFIX ?= riscv64-unknown-elf-
RISCV_GCC ?= $(RISCV_PREFIX)gcc
RISCV_GCC_OPTS ?= -march=$(MARCH) -mabi=$(MABI) -ffreestanding -nostartfiles -Os -fdata-sections -ffunction-sections --specs=nosys.specs -DFREQ_CLK_CORE=$(FREQ_CLK_CORE) -D$(PLATFORM)
# RISCV_BMARK_GCC_OPTS ?= -march=$(MARCH) -mabi=$(MABI) -ffreestanding -nostartfiles -static -std=gnu99 -O2 -ffast-math -fno-common -fno-tree-loop-distribute-patterns --specs=nosys.specs -DFREQ_CLK_CORE=$(FREQ_CLK_CORE) -D$(PLATFORM)
RISCV_OBJDUMP ?= $(RISCV_PREFIX)objdump --disassemble-all --disassemble-zeroes --section=.text --section=.text.startup --section=.text.init --section=.data
RISCV_SIM ?= spike

# Program and data memory specifications
PROGMEM_START ?= 0x00001000
PROGMEM_LENGTH ?= 64K
DATAMEM_START ?= 0x20000000
DATAMEM_LENGTH ?= 64K

PLATFORM ?= sim
FREQ_CLK_CORE ?= 50000000

# A-Core generated header files
A_CORE_HEADERS_PATH ?= ../a-core_thesydekick/Entities/ACoreChip/chisel/include
A_CORE_HEADERS = $(wildcard $(A_CORE_HEADERS_PATH)/*.h)
INCLUDES = -I$(A_CORE_HEADERS_PATH) -I$(src_dir)/include -I$(A_CORE_UTILS_INCLUDE_PATH)

# A-Core utility functions
A_CORE_LIB_PATH ?= ../a-core-library
A_CORE_UTILS_SOURCES = $(wildcard $(A_CORE_LIB_PATH)/a-core-utils/src/*.c)
A_CORE_UTILS_INCLUDE_PATH = $(A_CORE_LIB_PATH)/a-core-utils/include
A_CORE_UTILS_INCLUDES = $(wildcard $(A_CORE_UTILS_INCLUDE_PATH)/*.h)
A_CORE_LIB_INCLUDES = -I$(A_CORE_UTILS_INCLUDE_PATH) -I$(A_CORE_HEADERS_PATH)

DEFAULT_LINKER_PATH ?= ../a-core-library

LDOPTS = -T env_acore/a-core.ld -lc_nano -lm -Wl,--no-relax -static \
	-Wl,--defsym=PROGMEM_START=$(PROGMEM_START),--defsym=PROGMEM_LENGTH=$(PROGMEM_LENGTH),--defsym=DATAMEM_START=$(DATAMEM_START),--defsym=DATAMEM_LENGTH=$(DATAMEM_LENGTH) \
	-L$(DEFAULT_LINKER_PATH)

#------------------------------------------------------------
# Build assembly tests

# Saves the previous platform so that it know to recompile if it changes
.PHONY: force
platform_selection: force
	echo '$(PLATFORM)' | cmp -s - $@ || echo '$(PLATFORM)' > $@

%.dump: %
	$(RISCV_OBJDUMP) $< > $@

%.out: %
	$(RISCV_SIM) --isa=rv64gc_zfh_zicboz_svnapot_zicntr --misaligned $< 2> $@

%.out32: %
	$(RISCV_SIM) --isa=rv32gc_zfh_zicboz_svnapot_zicntr --misaligned $< 2> $@

$(src_dir)/Makefile:
	@cd $(src_dir) && ./Configure --cross-compile-prefix=$(RISCV_PREFIX) && make build_generated

aes-test: $(src_dir)/Makefile $(aes_src_files) $(common_src_files) $(A_CORE_HEADERS) a-core-utils.o env_acore/a-core.ld platform_selection
	$(RISCV_GCC) $(RISCV_GCC_OPTS) $(aes_src_files) $(common_src_files) -Wall -o $(PLATFORM).elf a-core-utils.o $(LDOPTS) -D$(PLATFORM) -DFREQ_CLK_CORE=$(FREQ_CLK_CORE) $(INCLUDES)

a-core-utils.o: $(A_CORE_HEADERS) $(A_CORE_UTILS_SOURCES) $(A_CORE_UTILS_INCLUDES)
	$(RISCV_GCC) -c $(RISCV_GCC_OPTS) $(A_CORE_UTILS_SOURCES) $(A_CORE_LIB_INCLUDES)


tests_dump = $(addsuffix .dump, $(tests))
tests_hex = $(addsuffix .hex, $(tests))
tests_out = $(addsuffix .out, $(filter rv64%,$(tests)))
tests32_out = $(addsuffix .out32, $(filter rv32%,$(tests)))

run: $(tests_out) $(tests32_out)

junk += $(tests) $(tests_dump) $(tests_hex) $(tests_out) $(tests32_out)

#------------------------------------------------------------
# Default

all: aes-test

#------------------------------------------------------------
# Clean up

clean:
	rm -rf $(junk)
